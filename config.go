package config

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	"gopkg.in/yaml.v2"
)

// Set - stores rf application Seturations
type Set struct {
	vars map[interface{}]interface{}
}

// Set
func (config *Set) Set(key string, value interface{}) (err error) {

	if config.vars == nil {
		config.vars = make(map[interface{}]interface{})
	}

	pMap := config.vars

	parts := strings.Split(key, ".")

	for idx, p := range parts {

		isLast := (idx == (len(parts) - 1))

		if isLast {
			pMap[p] = value
			break
		} else if ref, ok := pMap[p]; ok {
			if mapRef, mok := ref.(map[interface{}]interface{}); mok {
				pMap = mapRef
			} else {
				err = fmt.Errorf("can't set %s. Because %s is not a map", key, p)
			}
		} else {
			newMap := make(map[interface{}]interface{})
			pMap[p] = newMap
			pMap = newMap
		}

		if err != nil {
			break
		}
	}

	return
}

// GetString - returns config value as string
func (config *Set) GetString(name string) (value string, err error) {

	var val interface{}

	val, err = config.Get(name)

	if err != nil {
		return
	}

   if val == nil {
      val = ""
   }

	value = fmt.Sprintf("%v", val)

	return
}

// GetInt - returns config value as integer
func (config *Set) GetInt(name string) (value int, err error) {

	var val interface{}

	val, err = config.Get(name)

	if err != nil {
		return
	}

	value, ok := val.(int)

	if !ok {
		err = errors.New("variable is not an integer")
	}

	return
}

// GetInt64 - returns config value as integer 64
func (config *Set) GetInt64(name string) (value int64, err error) {

	var val interface{}

	val, err = config.Get(name)

	if err != nil {
		return
	}

	if v, ok := val.(int64); ok {
		value = v
	} else if v, ok := val.(int); ok {
		value = int64(v)
	} else if v, ok := val.(int16); ok {
		value = int64(v)
	} else if v, ok := val.(int8); ok {
		value = int64(v)
	} else if v, ok := val.(float32); ok {
		value = int64(v)
	} else if v, ok := val.(float64); ok {
		value = int64(v)
	} else {
		err = errors.New("variable is not an integer")
	}

	return
}

// GetDuration - returns config value as time.Duration
func (config *Set) GetDuration(name string) (value time.Duration, err error) {

	var sval string

	sval, err = config.GetString(name)

	if err != nil {
		return
	}

	r, _ := regexp.Compile("([0-9]+)(ns|ms|s|m|h|d)")

	result := r.FindStringSubmatch(sval)

	if len(result) != 3 {
		err = fmt.Errorf("invalid duration format: %s", sval)
		return
	}

	ival, err := strconv.Atoi(result[1])

	if err != nil {
		return
	}

	value = time.Duration(ival)

	switch string(result[2]) {
	case "ms":
		value *= time.Millisecond
	case "s":
		value *= time.Second
	case "m":
		value *= time.Second * 60
	case "h":
		value *= time.Second * 3600
	case "d":
		value *= time.Second * 86400
	}

	return
}

// GetBool - returns config value as boolean
func (config *Set) GetBool(name string) (value bool, err error) {

	var val interface{}

	value = false

	val, err = config.Get(name)

	if err != nil {
		return
	}

	value, ok := val.(bool)

	if !ok {
		err = errors.New("variable is not a boolean")
	}

	return
}

// GetSlice - returns config value as slice
func (config *Set) GetSlice(name string) (value []interface{}, err error) {

	var val interface{}

	val, err = config.Get(name)

	if err != nil {
		return
	}

	value, ok := val.([]interface{})

	if !ok {
		err = errors.New("variable is not a slice")
	}

	return
}

// GetMap - returns config value as map
func (config *Set) GetMap(name string) (value map[interface{}]interface{}, err error) {

	var val interface{}

	val, err = config.Get(name)

	if err != nil {
		return
	}

	value, ok := val.(map[interface{}]interface{})

	if !ok {
		err = errors.New("variable is not a map")
	}

	return
}

// Get - returns a value for Seturation variable if exists. Else returns empty string
func (config *Set) Get(name string) (value interface{}, err error) {

	var it interface{}

	if config == nil {
		panic("CONFIG IS NULL")
	}

	if config.vars == nil {
		config.vars = make(map[interface{}]interface{})
	}

	it = config.vars

	normalizedName := strings.Replace(strings.Replace(name, "[", ".", -1), "]", "", -1)
	nameComponents := strings.Split(normalizedName, ".")

	for _, n := range nameComponents {

		found := false

		if m, ok := it.(map[interface{}]interface{}); ok {
			it, found = m[n]
		} else if slice, ok := it.([]interface{}); ok {

			idx, err := strconv.Atoi(n)

			if err != nil || idx < 0 || idx >= len(slice) {
				found = false
			} else {
				it = slice[idx]
				found = true
			}
		}

		if !found {
			err = fmt.Errorf("variable does not exists in Set: %s", name)
			break
		}
	}

	value = it

	return
}

// Load - creates a new config set from config file
func Load(configFile string) (cs *Set, err error) {

	var file *os.File

	file, err = os.Open(configFile)

	if err != nil {
		return
	}

	defer file.Close()

	content, err := setupEnvVars(file)

	cs = &Set{
		vars: make(map[interface{}]interface{}),
	}

	reader := strings.NewReader(content)

	decoder := yaml.NewDecoder(reader)
	err = decoder.Decode(&cs.vars)

	return
}

// NewSet creates an empty config set
func NewSet() *Set {
	return &Set{
		vars: make(map[interface{}]interface{}),
	}
}

func setupEnvVars(file *os.File) (content string, err error) {

	s, err := ioutil.ReadAll(file)

	content = string(s)

	for _, e := range os.Environ() {

		p := strings.Split(e, "=")

      content = strings.Replace(content, fmt.Sprintf("${%s}", p[0]), strings.Join(p[1:], "="), -1)
	}

	return
}
