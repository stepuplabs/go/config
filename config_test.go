package config

import (
	"testing"
	"time"
   "os"
   "fmt"
)

func TestConfigRead(t *testing.T) {

   os.Setenv("CFG_SEC_VAR", "x=1,y=2")

	cs, err := Load("./config.yml.test")

	if err != nil {
		t.Error(err)
		return
	}

	ival, err := cs.GetInt("app.values.integer_value")

	if err != nil || ival != 5000 {
		t.Error("unexpected value for app.values.integer_value")
	}

	dval, err := cs.GetDuration("app.values.duration_value")

	if err != nil || dval != (time.Second*5) {
		t.Errorf("unexpected value for app.values.duration_value (%v != %v)! (ERROR: %v)", dval, (time.Minute * 5), err)
	}

	sval, err := cs.GetString("app.values.string_value")

	if err != nil || sval != "my great string" {
		t.Error("unexpected value for app.values.string_value")
	}

	slval, err := cs.GetSlice("app.values.slice_value")

	if err != nil || len(slval) != 4 {
		t.Error("unexpected length for app.values.slice_value")
	}

	slfirstval, err := cs.GetString("app.values.slice_value[0].name")

	if err != nil || slfirstval != "slice 0" {
		t.Error(err)
	}

	sllastval, err := cs.GetString("app.values.slice_value[3].name")

	if err != nil || sllastval != "slice 3" {
		t.Error(err)
	}

   envvar, err := cs.GetString("app.values.environment.myvar")

   if err != nil || envvar != "x=1,y=2" {
      t.Error(err)
   }

   secvar, err := cs.GetString("app.values.environment.secvar")

   if err != nil || secvar != "x=1,y=2" {
      if err != nil {
         t.Error(err)
      } else {
         t.Error(fmt.Errorf("unexpected value of secvar = [%s]", secvar))
      }
   }
}

func TestConfigWrite(t *testing.T) {

	cs := NewSet()

	cs.Set("val0", 1)
	cs.Set("val1", 2)
	cs.Set("app.values.ival", 1)
	cs.Set("app.values.sval", "str")
	cs.Set("app.values.bval", true)
	cs.Set("app.values.dval", time.Minute*5)

	val0, err := cs.GetInt("val0")

	if err != nil || val0 != 1 {
		t.Error(err)
		return
	}

	val1, err := cs.GetInt("val1")

	if err != nil || val1 != 2 {
		t.Error(err)
		return
	}

	ival, err := cs.GetInt("app.values.ival")

	if err != nil || ival != 1 {
		t.Error(err)
		return
	}

	sval, err := cs.GetString("app.values.sval")

	if err != nil || sval != "str" {
		t.Error(err)
		return
	}

	bval, err := cs.GetBool("app.values.bval")

	if err != nil || bval != true {
		t.Error(err)
		return
	}

	dval, err := cs.GetDuration("app.values.dval")

	if err != nil || dval != (time.Minute*5) {
		t.Error(err)
		return
	}
}
